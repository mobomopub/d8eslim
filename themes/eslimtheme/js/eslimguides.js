var jQuery32 = jQuery;
jQuery.noConflict(true);

(function ($, Drupal) {
  'use strict';

  /**
   * Example function.
   * @see https://www.drupal.org/node/304258#drupal-behaviors
   */
  Drupal.behaviors.accordions = {
    attach: function (context, settings) {
      $("#accordion").accordion({
        collapsible: true,
        active: false,
        heightStyle: "content",
        icons: false

      });
      $('#accordion > h2').click(function(e){
          var x = '#'+ this.id;
          $('.sidebar_container', context).find('a').each(function() {
              //console.log($(this).attr('href'));
              if ($(this).attr('href') == x) {
                  if (!$(this).hasClass('active')) {
                      $(this).addClass('active');
                  }
                  else {
                      $(this).removeClass('active');
                  }
          //alert($(this).attr("class"));
          //$('.sidebarItem').removeClass('active');

                  //console.log($(this).attr('href'));
            //      $(this).addClass('active');

              }
              else {
                  $(this).removeClass('active');
              }

          });
          return false;
      });
      $(".sidebarItem", context).on("click", function() {
          var $this = $(this),
          toOpen = $this.data("panel");
          $( "#accordion" ).accordion("option", "active", toOpen);
          var timer = $( "#accordion" ).accordion("option", "animate");
          setTimeout(function() {
          $("html,body").animate({
              scrollTop: $($("#accordion>div").get(toOpen)).offset().top
              }, 300);
          }, timer);
          $('.sidebarItem').removeClass('active');
          $(this).addClass('active');
          return false;
      });
    }
  };
}(jQuery32, Drupal));





