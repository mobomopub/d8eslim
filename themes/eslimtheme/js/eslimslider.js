var jQuery32 = jQuery;
jQuery.noConflict(true);

(function ($, Drupal) {

  'use strict';

  /**
   * Example function.
   * @see https://www.drupal.org/node/304258#drupal-behaviors
   */
  Drupal.behaviors.slickSlider = {
    attach: function (context, settings) {
      var backgrImg = $('[data-background]');

      if (backgrImg.length > 0) {
            backgrImg.each(function() {
              var $background, $backgroundmobile, $this;
              $this = $(this);
              $background = $(this).attr('data-background');
              $backgroundmobile = $(this).attr('data-background-mobile');
              if ($this.attr('data-background').substr(0, 1) === '#') {
                return $this.css('background-color', $background);
              } else if ($this.attr('data-background-mobile') && device.mobile()) {
                return $this.css('background-image', 'url(' + $backgroundmobile + ')');
              } else {
                return $this.css('background-image', 'url(' + $background + ')');
              }
            });
      }
    }
  };

}(jQuery32, Drupal));





