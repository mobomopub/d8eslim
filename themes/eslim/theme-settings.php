<?php

/**
 * @file
 * Provides an additional config form for theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

// Set theme name to use in the key values.
$theme_name = \Drupal::theme()->getActiveTheme()->getName();

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * Form override for theme settings.
 */
function eslim_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state) {
  $form['options_settings'] = [
    '#type' => 'fieldset',
    '#title' => t('Theme Specific Settings'),
  ];
  // Slick.js Support
  $form['options_settings']['slick'] = [
    '#type' => 'fieldset',
    '#title' => t('Slick Support'),
  ];
  $form['options_settings']['slick']['slick_enabled'] = [
    '#type' => 'checkbox',
    '#title' => t('Slick'),
    '#description' => t('Enable slick.js carousel (usage: .slick as wrapper)'),
    '#default_value' => theme_get_setting('slick'),
  ];

  // IE specific settings.
  $form['options_settings']['eslim_ie'] = [
    '#type' => 'fieldset',
    '#title' => t('Internet Explorer Stylesheets'),
  ];
  $form['options_settings']['eslim_ie']['ie_enabled'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable Internet Explorer stylesheets in theme'),
    '#default_value' => theme_get_setting('ie_enabled'),
    '#description' => t('If you check this box you can choose which IE stylesheets in theme get rendered on display.'),
  ];
  $form['options_settings']['eslim_ie']['ie_enabled_css'] = [
    '#type' => 'fieldset',
    '#title' => t('Which IE versions you want to enable ".lt-ie" CSS classes'),
    '#states' => [
      'visible' => [':input[name="ie_enabled"]' => ['checked' => TRUE]],
    ],
  ];
  $form['options_settings']['eslim_ie']['ie_enabled_css']['ie_enabled_versions'] = [
    '#type' => 'checkboxes',
    '#options' => [
      'ie8' => t('Internet Explorer 8'),
      'ie9' => t('Internet Explorer 9'),
    ],
    '#default_value' => array_keys(array_filter(theme_get_setting('ie_enabled_versions'))) ?: [],
  ];
}
