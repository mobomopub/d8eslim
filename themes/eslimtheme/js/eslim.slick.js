(function ($) {

  'use strict';

  /**
   * Example function.
   * @see https://www.drupal.org/node/304258#drupal-behaviors
   */
  Drupal.behaviors.slickslider = {
    attach: function (context, settings) {
              /* Slick settings for eslim theme */
              var slickCarousel = $('.ct-slick-homepage');
              if (slickCarousel.length > 0) {
                  slickCarousel.slick({
                      autoplay: false,
                      dots: true,
                  });
              }
    }
  };

})(jQuery);

