(function ($) {

  'use strict';

  // MatchHeight
  // For full settings see http://brm.io/jquery-match-height/
  // Usage:
  /**
    *  <ul>
    *		<li class="matchHeight">these will be equal heights</li>
    *		<li class="matchHeight">these will be equal heights</li>
    *		<li class="matchHeight">these will be equal heights</li>
    *	</ul>
    */
  Drupal.behaviors.matchHeight = {
    attach: function (context, settings) {
              /* matchHeight settings for eslim theme */
              var panel4col = $('.panel-col-4');
              var panel4colp = $('.panel-col-4 p');
              var matchHeight = $('.matchHeight');
              panel4col.matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
              });
              panel4colp.matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
              });
              matchHeight.matchHeight();
    }
  };

})(jQuery);
