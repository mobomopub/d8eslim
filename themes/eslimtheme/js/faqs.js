(function($, Drupal) {
  'use strict';

    $( "#accordion" ).accordion({
          collapsible: true,
          active: false,
          heightStyle: "content",
          icons: false

    });
    $(".sidebarItem").on("click", function() {
        var $this = $(this),
        toOpen = $this.data("panel");
        $( "#accordion" ).accordion("option", "active", toOpen);
        var timer = $( "#accordion" ).accordion("option", "animate");
        setTimeout(function() {
        $("html,body").animate({
            scrollTop: $($("#accordion>div").get(toOpen)).offset().top
            }, 300);
        }, timer);
        $('.sidebarItem').removeClass('active');
        $(this).addClass('active');
        return false;
    });
}(jQuery, Drupal));
